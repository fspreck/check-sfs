# Check compliance with a standard file structure

# Copyright (C) 2019-2020 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
# Authors:
# A. Schlemmer

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from yaml_header_tools import *
from glob import glob
import re
from datetime import datetime
from os.path import join

class Info(object):
    """
    An object representing meta data for a project folder.
    """
    def __init__(self):
        self.category = None
        self.project = None
        self.date = None
        self.descriptor = None

        self.description = None
        self.responsible = None

        self.results = None
        self.sources = None
        self.revisionOf = None

    def __repr__(self):
        date = ""
        descriptor = ""
        if self.date is not None:
            date = self.date
        if self.descriptor is not None:
            descriptor = "_" + self.descriptor
        return date + descriptor

# These have to be adapted if categories are added/renamed.
CATEGORIES = [
    "ExperimentalData",
    "SimulationData",
    "DataAnalysis",
    "Publications"]

def check_dir(basedir, subdir):
    """
    Check an individual directory.

    basedir is the root to the standard file structure.

    subdir is the actual directory, e.g.:
    ExperimentalData/2018_myproject/180214_test/
    """

    messages = []
    info = Info()

    # 1. Check the directory name:
    components = subdir.split(os.path.sep)
    if len(components) != 3:
        messages.append("Folder depth must be exactly 3.")
        return info, messages
    categories = CATEGORIES
    if components[0] not in categories:
        messages.append("Topmost folder is " + components[0] + " and not one of " + ",".join(categories) + ".")
    else:
        info.category = components[0]

    info.project = components[1]
        
    if components[0] == "Publications":
        m = re.match("([0-9]{4,4})(-[01][0-9]-[0-3][0-9])?_(.*)", components[2])
        if m is None:
            messages.append("Invalid folder name. Must be: yyyy(-mm-dd)_title")
        else:
            if m.group(2) is not None:
                try:
                    date = datetime.strptime(m.group(1) + m.group(2), "%Y-%m-%d")
                    info.date = m.group(1) + m.group(2)
                except ValueError as e:
                    messages.append("Invalid folder name. " + e.args[0])
            else:
                try:
                    date = datetime.strptime(m.group(1), "%Y")
                    info.date = m.group(1)
                except ValueError as e:
                    messages.append("Invalid folder name. " + e.args[0])

            info.descriptor = m.group(3)
    else:
        m = re.match("([0-9]{4,4}-[01][0-9]-[0-3][0-9])(_.*)?", components[2])
        if m is None:
            messages.append("Invalid folder name. Must be: yyyy-mm-dd(_descriptor)")
        else:
            try:
                datetime.strptime(m.group(1), "%Y-%m-%d")
                info.date = m.group(1)
            except ValueError as e:
                messages.append("Invalid folder name. " + e.args[0])
            if m.group(2) is not None:
                info.descriptor = m.group(2)[1:]

    # 2. Check the presence of README.md and its correctness:
    try:
        filename = join(basedir, subdir)
        header = get_header_from_file(filename)
        for f in ("responsible", "description"):
            if f not in header:
                messages.append("Mandatory field {} missing in {}".format(f, filename))
            else:
                setattr(info, f, header[f])
        for f in ("results", "sources", "revisionOf"):
            if f in header:
                setattr(info, f, header[f])
    except MetadataFileMissing as e:
        messages.append("Metadata file missing in {}".format(filename))
    except NoValidHeader as e:
        messages.append("No valid header in {}".format(filename))
    except ParseErrorsInHeader as e:
        messages.append("Errors in header of {}. {}".format(filename, e.reason))
    return info, messages

def identify_common_problems(res):
    """
    Check for some systematic problems within a resultlist produced by check_sfs.

    This function is used by check_sfs to generate more meaningful output.
    """

    # Check missing project folder:
    projectlist = [(r[1].category, r[1].project) for r in res]
    overview = dict()
    for p in projectlist:
        overview[p] = [0, 0, []]
    for i, r in enumerate(res):
        overview[(r[1].category, r[1].project)][1] += 1
        for msg in r[2]:
            if msg.startswith("Invalid folder name."):
                overview[(r[1].category, r[1].project)][0] += 1
                overview[(r[1].category, r[1].project)][2].append(i)
                break
    remind = []
    appmsg = []
    for k, v in overview.items():
        if v[0] > 0 and v[0] == v[1]:
            remind.extend(v[2])
            appmsg.append((join(*k), None, ["This folder is probably no project but an individual entry."]))
    res = [r for i, r in enumerate(res) if i not in remind]
    res.extend(appmsg)
    return res
    

def check_sfs(basedir, exclude=None):
    """
    Check a list of directories.

    exclude is a list of directories to exclude from the check. Path names in exclude
    should be given relative to the basedir.
    """

    if exclude is None:
        exclude = []
    dirlist = []
    for category in CATEGORIES:
        dirs = glob(join(basedir, category, "*", "*"))
        for exdir in exclude:
            dirs = [d for d in dirs if not d.startswith(join(basedir, exdir))]
        for d in dirs:
            index = d.find(category)
            dirlist.append((d[:index], d[index:]))

    res = []
    for directory in dirlist:
        res.append((directory[1], *check_dir(directory[0], directory[1])))
    return identify_common_problems(res)
