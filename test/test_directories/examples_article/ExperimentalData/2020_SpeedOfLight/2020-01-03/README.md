---
responsible:
- AuthorA
- AuthorB
description: Radio interferometry measurements to determine the speed of light
results:
- file: velocities.txt
  description: velocities of all measurements
...
