---
responsible: AuthorD
description: Average temperatures of the years 1990-1999 as obtained from wheatherdata.example
results:
- file: temperatures-199*.csv
  description: single year averages of all measurement stations with geographic locations
...
