#!/bin/python
# Tests for the tool using pytest
# A. Schlemmer, 04/2019

from check_sfs import check_dir, check_sfs
from os.path import join, dirname

def test_template_headers():
    # Will not be able to find folders:
    m = check_sfs(join(dirname(__file__), "test_directories/structure_a"))
    assert len(m) == 0

    m = check_dir(join(dirname(__file__), "test_directories/structure_a"),
                  "test_folder_1")
    assert m[1][0] == "Folder depth must be exactly 3."

def test_examples_article():
    m = check_sfs(join(dirname(__file__), "test_directories/examples_article"))
    assert len(m) == 14

    for r in m:
        assert len(r[2]) == 0
