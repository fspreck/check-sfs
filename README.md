# Check SFS

A tool to check a file structure for compliance with a standard file structure


This package has a yaml-header-tools as a dependency:
https://gitlab.com/salexan/yaml-header-tools



This python package can be installed using `pip`, e.g.:
```bash
pip install --user .
```

# Usage

## Check a single directory

```python
from check_sfs import check_dir

m = check_dir(join(dirname(__file__), "test_directories/structure_a"), "test_folder_1")
```

## Check a complete file structure

```python
from check_sfs import check_dir, check_sfs

m = check_sfs(join(dirname(__file__), "test_directories/structure_a"))
```

## Adding and renaming categories

Note that if categories such as `FieldData` or `MeasuredData` are to
be added to the structure, the list `CATEGORIES` in
`check_sfs/check_sfs.py` has to be adapted accordingly.

# Running the tests

After installation of the package run (within the project folder):

```bash
pytest
```


# Contributers

The original authors of this package are:

- Alexander Schlemmer
- Jan Lebert
- Florian Spreckelsen

# License

Copyright (C) 2019-2020 Research Group Biomedical Physics, Max Planck Institute for
Dynamics and Self-Organization Göttingen.

All files in this repository are licensed under a [GNU Affero General Public
License](LICENCE) (version 3 or later).
