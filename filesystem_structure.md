---
title: Standard File Structure
author: Alexander Schlemmer, Florian Spreckelsen, Baltasar Rüchardt
email: alexander.schlemmer@ds.mpg.de
subtitle: Version 1.1.2
date: 2020-10-19
numbersections: true
block-headings: true
geometry:
- left=25mm
- right=25mm
- top=20mm
- bottom=20mm
---

# TLDR

**Very short summary of the whole document:**

- Put your data in either: ExperimentalData, SimulationData, DataAnalysis or Publications
- Folder structure: `ExperimentalData/<project_identifier>/<date>[_OptionalIdentifer]`, for SimulationData and DataAnalysis analogously, for Publications see [Specific Details for Publications]
- Create the `README.md` file within that folder with **at least** the following content:

```
---
responsible: Your name
description: Short description of this data folder.
...
```

- Read the following sections for more details.


# Changelog

## 2020-10-19 Version 1.1.2

- This is a cleaned version which is now published at https://gitlab.com/salexan/check-sfs

## 2020-02-11 Version 1.1.1

- Specify naming convention for journal articles and adapt examples accordingly.

## 2019-09-11 Version 1.1

- Removal of tags `data` and `analysis`. Substitute for both is `sources`. (See [Notes] in [Recommended Header Attributes] for details.)
- Added more \* and ? for recommended attributes in [Recommended Header Attributes].
- All examples have been adapted to be in agreement with the above changes.

# Preface

This document a standardized file system structure which is also discussed in a publication: https://doi.org/10.3390/data5020043

Version 1.1.2 of this document reflects the state discussed in the article (https://doi.org/10.3390/data5020043). The specification might of course evolve including information from other workflows.
In case you encounter problems implementing this specification in your workflow, please send a message to **alexander.schlemmer@ds.mpg.de**. Please keep in mind that the specification proposed here was created with simplicity and universality in mind. It should be regarded as the minimum requirements for guaranteeing proper long-term storage, understandability and findability. Special workflows with more elaborated indexing approaches can always be built on top of it.

This specification was created and reviewed by:

- Alexander Schlemmer
- Florian Spreckelsen
- Baltasar Rüchardt
- Jan Lebert
- Sebastian Berg
- Johannes Schröder-Schetelig
- Jan-Hendrik Plank

The software CaosDB (https://caosdb.org) and especially the CaosDB file system crawler make use of this specification in the standard implementation of the crawler.
More information can be found in the repository of this implementation of the crawler: https://gitlab.com/henrik_indiscale/scifolder
The CaosDB file system crawler can be found in the following repository: https://gitlab.com/caosdb/caosdb-advanced-user-tools

In previous versions of this document there were plenty of examples included. As the article already contain lots of examples and the purpose of this document is more to serve as a specification
of the format, we recommend looking at https://doi.org/10.3390/data5020043 if examples are required.

# General Folder Structure

All folders found in this document can be considered subdirectories of a well-defined root directory which we also call base directory.

This base directory contains the following main folder structure:

| Folder Name       | Description                                                                   |
|-------------------|-------------------------------------------------------------------------------|
| ExperimentalData/ | Stores experimental data, e.g. camera recordings, lab notes, log files |
| SimulationData/   | Simulation scripts along with simulation results                              |
| DataAnalysis/     | Everything related to data analysis, e.g. plotting scripts, plotted figures   |
| Publications/     | Final publications of results, e.g. reports, talks, journal articles          |
|                   |                                                                               |

## Folder Substructure

The folder substructure in each of the main folders except for `Publications` is always the same:

`<project_identifier>/<date>[_OptionalIdentifier]/`

`<project_identifier>` is a unique identifier for your project and should be consistently used in all four main folders. If you are unsure what identifier to use, ask your supervisor, your colleagues or discuss it in the next lab meeting. It is important to use project identifiers with care to not lose the overview later. The project identifier has to include the year when the project was started. Valid project names have the form `YYYY_name_of_the_project`.

`<date>` is the date of the experiment, simulation or data analysis in the form YEAR-MONTH-DAY, e.g. 2019-04-17. See the corresponding sections for details how these dates are defined exactly.

`<OptionalIdentifier>` is an optional identifier that can be used to give a meaningful name to a specific folder. Furthermore it can be used in case of conflicts, when two experiments, data analyses, etc. were carried out on the same date. **Recommendation:** Often it is useful for experimental data to be able to refer to an existing experiment within a series with short and unique identifier. It is therefore desirable to assign a running number to your measurement and store it in the identifier. One example could be: `2019-05-09_Measurement1`, `2019-06-02_Measurement2`, ...

### Example 1

You created one run of a specific simulation in a series called "cell simulation":

`SimulationData/2019_cell_simulation/2019-04-17/` <- Put your files here!

### Example 2

You created two runs of a simulation in your optogenetics series. They have a feature that you want to use to distinguish them later. For example, they used different integration methods.

Two folders:

`SimulationData/2019_cell_simulation/2019-04-17_simple_euler/`

`SimulationData/2019_cell_simulation/2019-04-17_runge_kutta/`

### Example 3

If you created two runs of a simulation on the same day in your series and you do not know how to distinguish them, use an increasing number starting from 1:

Two folders:

`SimulationData/2019_cell_simulation/2019-04-17_1/`

`SimulationData/2019_cell_simulation/2019-04-17_2/`



### Example 4

Someone already had created one run of a simulation in the series and you want to store another one, that you created on the same day. This would result in two folders:

`SimulationData/2019_cell_simulation/2019-04-17/`

`SimulationData/2019_cell_simulation/2019-04-17_2/`

This is just one example to handle a conflict like this. Another possibility for the second folder would be:

`SimulationData/2019_cell_simulation/2019-04-17_PartTwo/`

# The Metadata file `README.md`

A file called `README.md` **MUST** be present in every folder on date-level, e.g. from the examples above in the folder   `SimulationData/2019_cell_simulation/2019-04-17_2/`. In this file metadata are stored and it has two main purposes:

- It should be possible identify the contents of this folder and a person that is responsible for it.
- It should store data which is required by the CaosDB crawler to index and link the data in this folder properly.

The file follows the markdown specification as implemented by pandoc:
[Pandoc Documentation](https://pandoc.org/MANUAL.html#pandocs-markdown).

There can be any contents in this file in markdown format.
Long descriptions of the contents are possible.
However, the header of the file is the most important part, as it can be read by machines.
The header is identified by two delimiters:


    Header begins with three dashes...

    ---

    ...
    and ends with three points.


This is also described in more detail in the pandoc manual for the
extension `yaml_metadata_block`. Note that if the header is not at the
beginning of the `README.md` it must be preceded by a blank line. Also
note that we're silghtly stricter concerning the delimeters: the
header really needs three dashes at its beginning and three dots at
its end, even though a yaml block in pandoc in principle can end with
three dashes as well. We also require exactly one header block.

The text between the header delimiters follows the YAML format.
See [YAML in Wikipedia](https://en.wikipedia.org/wiki/YAML) for details.

## Header Specification

The header has two mandatory fields for every main folder (ExperimentalData, SimulationData, DataAnalysis, Publications):


    ---
    responsible: Name of the responsible person or list of responsible persons
    description: Short description of this data folder.
    ...

### Example 1: Header with one responsible person


    ---
    responsible: Alexander Schlemmer
    description: 2d simulation of coupled cells
    ...

Note: If you write multiline text you need to indent the second and following lines by two spaces. Or use one of the other options specified in the yaml documentation.


### Example 2: Header with two responsible persons

    ---
    responsible: 
    - Alexander Schlemmer
    - Florian Spreckelsen
    description: 2d simulation of coupled cells
    ...

Note: If you write multiline text you need to indent the second and following lines by two spaces. Or use one of the other options specified in the yaml documentation.

## Recommended Header Attributes

For the specific main folders (**E**xperimentalData, **S**imulationData, **A**nalysis and **P**ublications) the following recommendations are given for the header :

| E | S | A | P | Header Attribute | Description                                                                                                     |
|---|---|---|---|------------------|-----------------------------------------------------------------------------------------------------------------|
|   |   |   |   |                  | **Links to different folders:**                                                                                 |
|   | ? | * | * | sources          | A list of files or directories identifying sources                                                              |
| ? | ? | ? | ? | revisionOf       | A directory name of a previous simulation, analysis or publication which is superseded by this folder           |
|   |   |   |   |                  |                                                                                                                 |
|   |   |   |   |                  | **Within the same folder:**                                                                                     |
|   | * | * | ? | scripts          | A list of files or directories identifying data analysis or simulation scripts                                  |
| * | * | * | ? | results          | A list of files or directories identifying results of the corresponding experiment, simulation or data analysis |


\* marks recommended tags.
? marks tags recommended only in some cases. Details follow in the next subsections.

### Notes
- The two rows below "Within the same folder" are used to list directories or files that are contained in the current folder.
- The two rows below "Links to different folders" are used to list directories or files that can be located anywhere in the file structure, but not within the same folder.
- Both, relative or absolute path formats are allowed in **both** cases. It just has to be made sure that the resolved path names are either within the current folder or not.

### File Glob Syntax

For specifying directories file globs can be used. The glob syntax described in the documentation of the Python
[glob module](https://docs.python.org/3/library/glob.html) is used in the recursive version.

### Notes on tag `sources`

#### Main folder analysis

The `README.md` header in analysis folders should contain an entry specifying the source data files or folders for this analysis. This can be either done very exactly by pointing to one or more files, or rather fuzzy by
pointing to whole directories. **In any case pointing to fuzzy sources is much more useful than not specifying the sources at all!**

Pointing at a whole directory will link the analysis only to the
corresponding experiment/simulation/analysis, while pointing at
individual files will create a link to the source
experiment/simulation/analysis **and** the entries belonging to the
individual files in CaosDB.

#### Main folder Publications

Similar to the analysis, the header of publications should contain entries for specifying the source data files or folders for the publication. In addition it should contain entries for specifying the data analysis files or folders
this publication is based on.

### Notes on tags: results and scripts

Simulations and data analysis consist very often of two parts:

- One scripting part that generates results and contains parameters or information about input files.
- The actual results of either the simulation or the data analysis.

The two tags `results` and `scripts` are intended to declare, which of the files in the folder are scripts
and which results.

Analogous to `sources` these tags can either be single files, folders, lists of files and folders
or statements following the glob syntax described above.

Many people already organize their files in accordingly named directories. In these cases these two
tags can be simply set to:

    results: results/**
    scripts: scripts/**

**Note**: This will choose all files in the subfolders results/ and scripts/ *relativly to the folder where the README.md is saved*.

### Individual names for files or folders

If you want to give more information for your files you can use an extended syntax:

    scripts:
    - filename: scripts/plot.py
      description: Creates a nice time series plot for the data files
    - filename: scripts/test_plot.py
      description: Unit test functions for plot.py
      
**Note**: The attribute *description* now is intended by two spaces. This is necessary to associate it with `filename` instead of interpreting it as a new tag.  

You can also use a mixture of described statements and simple statements:
      

    scripts:
    - filename: scripts/plot.py
      description: Creates a nice time series plot for the data files
    - filename: scripts/test_plot.py
      description: Unit test functions for plot.py
    - scripts/modules/**
    - scripts/create*.*


#### Existing RecordTypes in CaosDB

There is one optional extension of the `filename`-`description`
structure if the referenced file or folder belongs to an already
existing RecordType in CaosDB. This will most likely be the case if
you're adding another simulation/experiment/analysis similar to one
already present in the database, e.g., when continuing one specific
experimental series. In this case you can add the specific RecordType
via the `recordtype` tag:

	- filename: <filename.ending>
	  description: <description>
	  recordtype: <RecordType>
	  
CaosDB will then use this RecordType to classify the file in
filename. The easiest way to find out the specific RecordType in the
database is currently to open one of the already existing
simulations/experiments/... in e.g. the CaosDB web interface and copy
it from there. Otherwise, you can ask your colleagues, check the UML
or the RecordType list of CaosDB, or talk to the maintainer of your
CaosDB instance. Again, the `recordtype` tag is optional and can
safely be ignored if you're not using CaosDB at all.

### Notes on tag: revisionOf

Sometimes it can happen that you find a mistake in an analysis, publication or simulation that has already been submitted to CaosDB. In this case, **please don't just correct the mistake in the original folder**! In fact, after a folder is indexed within CaosDB it should not be touched anymore. CaosDB will detect all changes to files already present, and throw a warning.

Instead, please copy the old folder, correct the mistake (including all dependencies, such as generated data and data analysis output files) and add the `rivisionOf` tag to its `README.md`.

The `revisionOf` tag is the full path to a folder containing the old version of this analysis, publication or simulation.

# Specific Details for ExperimentalData

## Tag: results

As for simulations and analysis, the result tag can (and should) be used to list the files containing the results of the experiment; ideally with a short description, too. Use the `filename`, `description` syntax explained in [Individual names for files or folders] for this.

# Specific Details for SimulationData

## Date

The date of a simulation is difficult to define. In fact, it is not important whether the meaning of the date within the folder structure is very strictly defined. The purpose of the date in the filename is rather, to give the visitor a hint on when approximately a simulation was created.

A general recommendation is the date when a simulation was started.

## Simulations with too big data folders

In many cases simulations can be uniquely reconstructed using a script.
Some simulations create huge amounts of data. In these cases it might be worth to consider not submitting
the raw data to the file system at all, but only keep processed data, or even only the script.
**In this case it is absolutely important to carefully check, whether the data really can be uniquely reconstructed using the simulation script!**

If simulations have a very long runtime and the amount of data is still in a reasonable range, it might be still worth to keep the raw data along with all simulation programs.

# Specific Details for DataAnalysis

## Date

Similar to simulation data, the date of a data analysis is difficult to define.
A general recommendation is to use the initial date of a data analysis task.

# Specific Details for Publications

Publications are probably not as frequently done as experiments, simulations and data analysis tasks. Therefore it makes sense to use a different structure for them.

There are the following subdirectories within `Publications/`:

| Folder name    | description                                                       |
|----------------|-------------------------------------------------------------------|
| Theses/        | Bachelor-, Master-, PhD-Theses                                    |
| Articles/      | Journal Articles                                                  |
| Posters/       | Posters, e.g. for conferences or SAB meetings                     |
| Presentations/ | Talks, e.g. for conferences / lab meetings                        |
| Reports/       | Collections of results, e.g. for leap meetings, group discussions |

Within these folders, the subfolders should be created as follows:

`<date>_<description>`

`<date>` can be abbreviated, e.g. be only the year. 
`<description>` should be a very short description of the project. In
case of journal articles, the description should at least include the
first author's name and the (short) title of the journal.
